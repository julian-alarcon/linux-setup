alias gc="git fetch -p"
alias k8sdev="kubectl --context ew1d1 --namespace=platform-api-dev"
alias k8sprod="kubectl --context ew1p1 --namespace=platform-api"
alias k8sgreen="kubectl --context ew1p3 --namespace=platform-api"
alias k8sgreen1="kubectl --context ew1p1 --namespace=platform-api"
alias k8sdevas1="kubectl --context as1d1 --namespace=platform-api-dev"
alias gu="git checkout master && git pull && git branch && gc"
alias code="codium"
