# ~/.bashrc: executed by bash(1) for non-login shells

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)                                                                                          
HISTSIZE=10000
HISTFILESIZE=20000

# setting byobu to start always
[ -r /home/jalarcon/.byobu/prompt ] && . /home/jalarcon/.byobu/prompt   #byobu-prompt#
