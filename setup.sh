#!/usr/bin/env bash
set -uo pipefail;

#######################################
# Install Ansible 2.16.6
# Arguments:
#   None
#######################################
function install_ansible() {
    sudo apt purge ansible -y
    sudo apt purge ansible-core -y
    sudo apt update
    sudo apt install ansible-core -y
    sudo wget -O /var/cache/apt/archives/ansible-core.deb https://ppa.launchpadcontent.net/ansible/ansible/ubuntu/pool/main/a/ansible-core/ansible-core_2.16.6-1ppa~noble_all.deb && sudo dpkg -i /var/cache/apt/archives/ansible-core.deb && sudo apt --fix-broken install -y && sudo apt purge ansible -y
}

# ref: https://askubuntu.com/a/970898/14799
if ! [ $(id -u) = 0 ]; then
    echo "The script need to be run as root." >&2
    exit 1
fi

if [ $SUDO_USER ]; then
    real_user=$SUDO_USER
else
    real_user=$(whoami)
fi

# Check if ansible version is compatible, if not then install a newer version

ansible_version=$(ansible --version | grep 'ansible \[core' | sed -E 's/.*ansible \[core (.*)\].*/\1/')

# Regex made with ChatGPT
if [[ $ansible_version =~ ^(2\.[1-9][3-9]|[3-9]|[1-9][0-9]+)\..* ]]; then
    echo "Compatible version for Ansible was found, version $ansible_version, process will continue..."
else
    echo "Ansible version $ansible_version is not compatible. Installing compatible version..."
    install_ansible
fi

# # Install Ansible roles

ansible-galaxy collection install community.general
ansible-playbook playbook.yml -i inventory.yml -vvv
