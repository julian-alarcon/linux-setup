# Setup of Linux using Ansible Roles

[[_TOC_]]

## Supported Operating Systems

* Ubuntu 24.04

## Instructions

Execute the setup.sh script with sudo command

```sh
sudo ./setup.sh
```

## To Automate

### Pending installations to setup

* minikube

### Settings

> Check differences `dconf dump / > before.txt`  and then `dconf dump / > after.txt`, `diff before.txt after.txt`

* set dark
    * `gsettings set org.gnome.desktop.interface color-scheme prefer-dark`
    * `gsettings set org.gnome.desktop.interface gtk-theme 'Yaru-dark'`
* font sizes
    * `gsettings set org.gnome.desktop.interface font-name 'Ubuntu 10'`
    * `gsettings set org.gnome.desktop.interface document-font-name 'Sans 10'`
    * `gsettings set org.gnome.desktop.interface monospace-font-name 'Ubuntu Mono 12'`
    * `gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Ubuntu Bold 10'`
    * `gsettings set org.gnome.desktop.interface text-scaling-factor 1.25` - Extra, related to usage in big screens
* dock: autohide, size, appearance and position
    * `gsettings set org.gnome.shell.extensions.dash-to-dock dock-fixed false`
    * `gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 48`
    * `gsettings set org.gnome.shell.extensions.dash-to-dock extend-height false`
    * `gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM`
* add `type` column on Nautilus
    * `gsettings set org.gnome.nautilus.list-view default-visible-columns "['name', 'size', 'type', 'date_modified_with_time']"`
* set list view on Nautilus
    * `gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'`
* default order by type in Nautilus
    * `gsettings set org.gnome.nautilus.preferences default-sort-order 'type'`
* Show hidden files`````~~~~~~
    * `gsettings set org.gnome.nautilus.preferences show-hidden-files true`
* Sort first directories in Nautilus
    * `gsettings set org.gtk.Settings.FileChooser sort-directories-first true`
* Miscellaneous Settings
    * `gsettings set com.ubuntu.update-manager show-details true`
* Keyboard Setup
    * `gsettings set org.gnome.desktop.input-sources xkb-options "['compose:ralt']"`
* show battery percentage
    * `gsettings set org.gnome.desktop.interface show-battery-percentage true`
* create directories
    * `mkdir -p ~/Code`
* [clipboard history extension](https://extensions.gnome.org/extension/4839/clipboard-history/)
* byobu setup
* codium settings
* For ISO Keyboards: GNOME Tweaks -> Keyboard & Mouse -> Additional Layout Options:
    * Key to choose the 2nd level: The "< >" key
* default vlc for videos/audio (`~/.config/mimeapps.list` file or `gnome-control-center default-apps`)
    * `xdg-mime default vlc.desktop audio/mp4`
    * `xdg-mime default vlc.desktop video/webm`
* clock settings and locations
    * `gsettings set org.gnome.desktop.interface clock-show-date true`
    * `gsettings set org.gnome.desktop.interface clock-show-weekday true`
    * `gsettings set org.gnome.shell.world-clocks locations "[<(uint32 2, <('Bogotá', 'SKBO', true, [(0.082321368870163392, -1.2941616403537954)], [(0.080285145591739146, -1.2929980817013682)])>)>, <(uint32 2, <('Berlin', 'EDDT', true, [(0.91746141594945008, 0.23241968454167572)], [(0.91658875132345297, 0.23387411976724018)])>)>, <(uint32 2, <('Paris', 'LFPB', true, [(0.85462956287765413, 0.042760566673861078)], [(0.8528842336256599, 0.040724343395436846)])>)>, <(uint32 2, <('New Delhi', 'VIDP', true, [(0.49858239547081096, 1.345939747314058)], [(0.49916416607037828, 1.3473941825396225)])>)>]"`
    * `gsettings set org.gnome.clocks world-clocks "[{'location': <(uint32 2, <('Bogotá', 'SKBO', true, [(0.082321368870163392, -1.2941616403537954)], [(0.080285145591739146, -1.2929980817013682)])>)>}, {'location': <(uint32 2, <('Berlin', 'EDDT', true, [(0.91746141594945008, 0.23241968454167572)], [(0.91658875132345297, 0.23387411976724018)])>)>}, {'location': <(uint32 2, <('Paris', 'LFPB', true, [(0.85462956287765413, 0.042760566673861078)], [(0.8528842336256599, 0.040724343395436846)])>)>}, {'location': <(uint32 2, <('New Delhi', 'VIDP', true, [(0.49858239547081096, 1.345939747314058)], [(0.49916416607037828, 1.3473941825396225)])>)>}]"`
* Set bash setup from `.bash_aliases` and `.bashrc` files
* set language English
* gnome window
    * `gsettings set org.gnome.desktop.wm.preferences auto-raise 'true'`
    * `gsettings set org.gnome.desktop.wm.preferences focus-new-windows 'strict'`
    * [https://gist.github.com/grenade/6363978](https://gist.github.com/grenade/6363978)
* vlc (fix subtitle)
* Git Setup
    * `sudo apt install libsecret-1-0 libsecret-1-dev libglib2.0-dev`
    * `sudo make --directory=/usr/share/doc/git/contrib/credential/libsecret`
    * `git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret`
    * `git config --global init.defaultBranch main`
    * `git config --global user.name "Julian Alarcon"`
    * `git config --global user.email "alarconj@gmail.com"`

#### VSCodium / VSCode

* Extensions:
    * Gitlab (need to logout/login when token is updated)
    * Terraform
    * GitLens
        * Set Remote (WORK)
    * markdownlint
    * yaml

* Settings

```json
// settings.json
{
    "redhat.telemetry.enabled": false,
    "terminal.integrated.allowChords": false,
    "gitlens.remotes": [{ "domain": "git.flix.tech", "type": "GitLab" }],
    "markdownlint.config": {
        "MD013": {
            "code_blocks": false,
            "tables": false
        },
        "MD007": {
            "indent": 4
        }
    },
    "editor.copyWithSyntaxHighlighting": false,
    "files.associations": {
        "*.yaml.tpl": "yaml",
        "*.yml.tpl": "yaml"
    },
    "yaml.schemas": {
        "https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json": "*.gitlab-ci*.yml"
    }
}
```

#### Darktable

##### Configurations

* general
    * theme: darktable-icons-grey
* miscellaneous
    * mouse wheel scrolls modules side panel by default: ✔️
* darkroom:
    * patterns for the image information line: `$(FILE_NAME).$(FILE_EXTENSION) • $(EXIF_EXPOSURE) • f/$(EXIF_APERTURE) • $(EXIF_FOCAL_LENGTH) mm • $(EXIF_ISO) ISO`
* processing:
    * auto-apply chromatic adaptation defaults: modern
    * auto-apply sharpen: ❌

* Lightable screen:
    * Star -> permanent extended overlays
* Darkroom screen:
    * module order -> v3.0
* Export -> Preferences: exif data, metadata, geo tags

##### Styles

Import from files darktable/styles

* [dt.styles](https://github.com/jade-nl/dt.styles/releases/download/v1.7/dt.all.colour.zip)
* personal
    * fisheye-correction (7 Artisans 7.5mm f2.8)

##### Lens/Cameras database

Update Database downloading to `~/.local/share/lensfun/updates/version_1` the
xml files from [lensfun repo](https://github.com/lensfun/lensfun/tree/master/data/db)

##### Modules

###### julians's workflow 2.0

* lens correction
* rotate and perspective
    * rotation
* crop
    * aspect
* color calibration (zoom out)
    * CAT
        * illuminant: custom
        * hue
        * chroma
* exposure
    * exposure
* haze removal
* filmic rgb
    * scene tab
        * white relative exposure: picker
        * black relative exposure
    * look tab
        * contrast
* raw chromatic aberration
* color balance rgb
    * master tab
        * global chroma
        * global saturation
        * global vibrance
    * masks tab
        * white fulcrum: picker
    * 4 ways
        * power
            * luminance
        * global offset
            * luminance
* tone equalizer
    * masking tab
        * mask exposure compensation: picker
    * advanced tab: scroll over image
* contrast equalizer (deblur)
* denoise (profiled)
    * profile: ILCE-7C + iso
    * mode: non-local means auto
* vignetting
    * exposure (oval mask for vignetting)
    * color balance rgb (same oval to and reduce saturation)
